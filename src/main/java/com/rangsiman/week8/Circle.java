package com.rangsiman.week8;

public class Circle {
    private double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    //Circle Area
    public double AreaCircle() {
        return 3.14*(radius*radius);
    }

    //Circle Perimeter
    public double PerimeterCircle() {
        return 2*3.14*radius;
    }
}
