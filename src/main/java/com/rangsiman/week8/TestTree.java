package com.rangsiman.week8;

public class TestTree {
    public static void main(String[] args) {
        Tree tree1 = new Tree(5, 10);
        System.out.println("Position tree1 = " + tree1.TreePositionX() + ", " + tree1.TreePositionY());

        Tree tree2 = new Tree(5, 11);
        System.out.println("Position tree2 = " + tree2.TreePositionX() + ", " + tree2.TreePositionY());
    }
}
