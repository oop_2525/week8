package com.rangsiman.week8;

public class TestShape {
    public static void main(String[] args) {
        Rectangle rect1 = new Rectangle(10, 5);
        System.out.println("Area rect1 = " + rect1.AreaRectangle());
        System.out.println("Perimeter rect1 = " + rect1.PerimeterRectangle());

        Rectangle rect2 = new Rectangle(5, 3);
        System.out.println("Area rect2 = " + rect2.AreaRectangle());
        System.out.println("Perimeter rect2 = " + rect2.PerimeterRectangle());

        Circle circle1 = new Circle(1);
        System.out.println("Area circle1 = " + circle1.AreaCircle());
        System.out.println("Perimeter circle1 = " + circle1.PerimeterCircle());

        Circle circle2 = new Circle(2);
        System.out.println("Area circle2 = " + circle2.AreaCircle());
        System.out.println("Perimeter circle2 = " + circle2.PerimeterCircle());

        Triangle triangle1 = new Triangle(5, 5, 6);
        System.out.println("Area triangle1 = " + triangle1.AreaTriangle());
        System.out.println("Perimeter triangle1 = " + triangle1.PerimeterTriangle());
    }
}

