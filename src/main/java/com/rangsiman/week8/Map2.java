package com.rangsiman.week8;

public class Map2 {
    private String name;
    private int x;
    private int y;
    public final static int X_MIN = 0;
    public final static int X_MAX = 10;
    public final static int Y_MIN = 0;
    public final static int Y_MAX = 10;

    public Map2(String name, int x, int y) {
        this.name = name;
        this.x = x;
        this.y = y;
    }

    public Map2(String name) {
        this(name, 0, 0);
    }
    
    public void print() {
        System.out.println(name + " x:" + x + " y:" + y);
    }

    // Getter Setter Method
    public String getName() {
        return name;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
