package com.rangsiman.week8;

public class Triangle {
    private int a;
    private int b;
    private int c;

    public Triangle(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    //Triangle Area
    public double AreaTriangle() {
        double s = (a+b+c)/2;
        return Math.sqrt(s*(s-a)*(s-b)*(s-c));
    }

    //Triangle Perimeter
    public double PerimeterTriangle() {
        return a+b+c;
    }
}
