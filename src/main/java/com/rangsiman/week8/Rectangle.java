package com.rangsiman.week8;

public class Rectangle {
    private int width;
    private int height;

    public Rectangle(int width, int height) {
        this.width = width;
        this.height = height;
    }

    //Rectangle Area
    public double AreaRectangle() {
        return width*height;
    }

    //Rectangle Perimeter
    public double PerimeterRectangle() {
        return (width+height)*2;
    }
}
